import { HttpClient, HttpHeaders, JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Todo } from './todo';

@Component({
  selector: 'app-mytodo',
  templateUrl: './mytodo.component.html',
  styleUrls: ['./mytodo.component.css']
})
export class MytodoComponent implements OnInit {
  private name: string = "Name";
  inputText: string = "";
  displayAll: boolean = false;
  todos: Todo[] = [];
  uri : string = "http://localhost:3000/todos";

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    debugger;
    this.http.get<Todo[]>(this.uri).subscribe(data => {
      this.todos = data;
    })
  }

  getName(){
    return this.name;
  }

  getTodos(){
    if(this.displayAll){
        return this.todos;
    }
    let todos = this.todos.filter(x => !x.action);
    return todos;
  }

  addTodo(){
    if(this.inputText != ""){
      const headers = { 'content-type': 'application/json'}
      let data = JSON.stringify({description:this.inputText,action:false});
      this.http.post<Todo>(this.uri,data,{'headers':headers}).subscribe(val => {
        console.log("POST Call succeful value returned in body",val,typeof(val));
        this.todos.push(val);
        this.inputText = "";
      },
      response => {
        console.log("POST call in eror",response);
      },
      () => {
        console.log("The POST observable is now complated");
      }
      );
    }
  }

  onActionChange(todoItem: Todo){
    const headers = {'content-type':'application/json'};
    let data = JSON.stringify(todoItem);
    this.http.post<Todo>(this.uri+`/${todoItem.id}`,data,{'headers':headers}).subscribe(val => {
      console.log("Put call succeful value returned in body2",val);
      this.todos.forEach(item => {
        if(item.id === todoItem.id){
          item.action = todoItem.action;
        }
      });
      // this.todos = this.todos.reduce((acc,item) => {
      //   if(item.id === todoItem.id){
      //     item.action = todoItem.action;
      //   };
      //   return[...acc,item];
      // },[])
    },
    response => {
      console.log("Put call in error ",response);
    },
    ()=> {
      console.log("The Put observvable is now complated");
    })

  }
  trackByFn(index:number,item: any){
    return index;
  }

}
